python 3.6 is required

pip install -r requirements.txt

run 'python inference.py'

Issues running the chatbot is often tied to difficulty installing tensorflow
Consider following a guide on https://www.tensorflow.org/install/
or consider running the program on ubuntu 16.04 with python 3.6